<html>
    <head>
        <title>Session 10</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1>Registration Form</h1>
                    <form class="form" action="registration.php" method="get">
                        <div class="form-group">
                                <label for="name">Name: </label>
                                <input type="text" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="username">Username: </label>
                            <input type="text" id="username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="email">Email: </label>
                            <input type="email" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="password">Password: </label>
                            <input type="password" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="re-password">Re-Password: </label>
                            <input type="password" id="re-password" name="re_password">
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender: </label>
                            Male: <input type="radio" name="gender" value="MALE">
                            Female: <input type="radio" name="gender" value="FEMALE">
                        </div>
                        <div class="form-group">
                            <h4>Your Favorite color</h4>
                             <p> <input type="checkbox" name="color[]" value="RED">Red</p><br>
                             <p> <input type="checkbox" name="color[]" value="BlUE">Blue</p><br>
                             <p> <input type="checkbox" name="color[]" value="GREEn">Green</p><br>
                             <p> <input type="checkbox" name="color[]" value="BLACK">Black</p><br>
                        </div>
                        <div class="form-group">
                            <h4>Select your country </h4>
                            <select name="country[] multiple="multiple " ">
                                <option value="USA">USA</option>
                                <option value="Egypt">Egypt</option>
                                <option value="KSA">KSA</option>
                                <option value="Netherkand">Netherland</option>
                                <option value="Deutchland">Deutchland</option>
                            </select> 
                        </div>
                        <div class="form-group">
                            <h4>Write a description about yourself </h4>
                             <textarea name="description" rows="10" cols="40x"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" value="submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>
</html>
