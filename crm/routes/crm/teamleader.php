<?php

Route::group(['middleware' => ['auth', 'TeamLeader']], function () {
    Route::get('/leads', 'LeadController@index')->name('leads.index');
    Route::get('/leads/{lead}', 'LeadController@show')->name('leads.show');
    Route::post('/leads/assigned', 'LeadController@assigned')->name('leads.assigned');
    Route::post('leads/call/{lead}','CallController@store')->name('calls.store');
    Route::get('/leads/call/{lead}','LeadController@call')->name('leads.call');
    Route::get('/calls','CallController@view')->name('calls.view');
    Route::get('/users','UserController@index')->name('users.index');


});