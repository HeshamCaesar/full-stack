<?php
Route::group(['middleware' => ['auth', 'Sales']], function () {
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/leads/call/{lead}','LeadController@call')->name('leads.call');
Route::post('leads/call/{lead}','CallController@store')->name('calls.store');
Route::get('/calls','CallController@view')->name('calls.view');
Route::get('/lead/my_lead','SalController@index')->name('sales.index');
Route::get('/call/my_calls','SalController@viewCalls')->name('sales.view');

});