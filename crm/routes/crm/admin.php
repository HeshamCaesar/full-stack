<?php
Route::group(['middleware' => ['auth', 'Admin']], function () {
    Route::resource('/leads', 'LeadController');
    Route::post('/leads/assigned', 'LeadController@assigned')->name('leads.assigned');
    Route::get('/leads/call/{lead}','LeadController@call')->name('leads.call');
    Route::Put('/leads/call/{lead}','CallController@store')->name('calls.store');
    Route::resource('/calls','CallController');
    Route::get('/calls','CallController@view')->name('calls.view');
    Route::get('/users/create','UserController@create')->name('users.create');
    Route::post('/users/create','UserController@store')->name('users.store');
});
