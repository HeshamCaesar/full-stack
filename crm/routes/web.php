<?php

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

// Admins routes
require_once ('crm\admin.php');
// Team leader routes
require_once ('crm\teamleader.php');
// Sales routes
require_once ('crm\sales.php');
Route::resource('/users', 'UserController');


// Errors
Route::get('/403', function () {
    return view('errors.403');
})->name('403');
