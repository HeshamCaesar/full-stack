<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-danger elevation-4" style="background:#004a99">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <i class="nav-icon fas fa-business-time"></i>
        <span class="brand-text font-weight-light">CRM</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                    @if(Auth::user()->role->slug=='ADM'||Auth::user()->role->slug=='TMLD')
                        <a href="#" class="nav-link {{ Request::is('leads*') ? 'active': ''}}">
                        <i class="nav-icon fas fa-chess-king"></i>
                        <p>
                            Leaders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    @endif
                    @if(Auth::user()->role->slug=='SAL')
                        <a href="#" class="nav-link {{ Request::is('lead*') ? 'active': ''}}">
                        <i class="nav-icon fas fa-chess-king"></i>
                        <p>
                            Leaders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    @endif

                    <ul class="nav nav-treeview">
                        @if (Auth::user()->role->slug == 'ADM')
                            <li class="nav-item">
                                <a href="{{ route('leads.create') }}" class="nav-link {{ Request::is('leads/create') ? 'active': ''}}">
                                    <i class="far fa-check-circle nav-icon"></i>
                                    <p>New Lead</p>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->role->slug == 'ADM' || Auth::user()->role->slug == 'TMLD')
                            <li class="nav-item">
                                <a href="{{ route('leads.index') }}" class="nav-link {{ Request::is('leads') ? 'active': ''}}">
                                    <i class="far fa-check-circle nav-icon"></i>
                                    <p>All Leads</p>
                                </a>
                            </li>
                        @elseif(Auth::user()->role->slug == 'SAL')
                                <li class="nav-item">
                                    <a href="{{ route('sales.index') }}" class="nav-link {{ Request::is('lead/my_lead') ? 'active': ''}}">
                                        <i class="far fa-check-circle nav-icon"></i>
                                        <p>My Leaders</p>
                                    </a>
                                </li>
                        @endif
                    </ul>
                </li>
                @if(Auth::user()->role->slug=='ADM')

                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link {{ Request::is('users*') ? 'active': ''}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Users
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('users.create')}}" class="nav-link {{ Request::is('users/create') ? 'active': ''}}">
                                <i class="far fa-check-circle nav-icon"></i>
                                <p>New User</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('users.index')}}" class="nav-link {{ Request::is('users') ? 'active': ''}}">
                                <i class="far fa-check-circle nav-icon"></i>
                                <p>All Users</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                @if (Auth::user()->role->slug == 'ADM'
                    || Auth::user()->role->slug == 'TMLD'
                    || Auth::user()->role->slug == 'SAL')
                    <li class="nav-item has-treeview menu-open">
                       @if(Auth::user()->role->slug=='ADM'||Auth::user()->role->slug=='TMLD')
                         <a href="#" class="nav-link {{ Request::is('calls*') ? 'active': ''}}">
                            <i class="nav-icon fas fa-phone-alt"></i>
                            <p>
                                Calls
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        @endif
                           @if(Auth::user()->role->slug=='SAL')
                               <a href="#" class="nav-link {{ Request::is('call*') ? 'active': ''}}">
                                   <i class="nav-icon fas fa-phone-alt"></i>
                                   <p>
                                       Calls
                                       <i class="right fas fa-angle-left"></i>
                                   </p>
                               </a>
                            @endif
                               <ul class="nav nav-treeview">
                            <li class="nav-item">
                            @if(Auth::user()->role->slug=='ADM'||Auth::user()->role->slug=='TMLD')
                                    <a href="{{route('calls.view')}}" class="nav-link {{ Request::is('calls*') ? 'active': ''}}">
                                    <i class="far fa-check-circle nav-icon"></i>
                                    <p>All calls</p>
                                </a>
                            @endif
                            @if(Auth::user()->role->slug=='SAL')
                                <a href="{{route('sales.view')}}" class="nav-link {{ Request::is('call/my_calls') ? 'active': ''}}">
                                    <i class="far fa-check-circle nav-icon"></i>
                                    <p>My calls</p>
                                </a>
                            @endif
                            </li>
                        </ul>
                    </li>
                    @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
