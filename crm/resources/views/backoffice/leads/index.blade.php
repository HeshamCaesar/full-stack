@extends('layouts.app')

@section('title', ' Leaders')

@section('pageHeader', 'All  Leaders')

@section('content')


    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Sales</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leads as $lead)
        <tr>
            <td>{{ $lead->id }}</td>
            <td>{{ $lead->name }}</td>
            <td>{{ $lead->phone }}</td>
            <td>{{ $lead->email }}</td>
            <td>
                {{--{!! $lead->sales? $lead->sales->name : '<span class="alert alert-danger">unassigned</span>' !!}--}}
                <form action="{{ route('leads.assigned') }}" method="post" class="text-center">
                    @csrf
                    <input type="hidden" name="lead" value="{{ $lead->id }}">
                    <select name="sales" id="sales" class="form-control" required>
                        <option value="0">Unassigned</option>
                        @foreach($sales as $user)
                            @if ($lead->sales && $lead->sales->id === $user->id)
                                <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                            @else
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endif
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary m-2">Save</button>
                </form>
            </td>
            <td>
                <a href="{{ route('leads.show', $lead) }}" class="btn btn-info">Show</a>
                @if (Auth::user()->role->slug == 'ADM')
                    {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-assigned">--}}
                        {{--Assigned--}}
                    {{--</button>--}}

                @endif
                @if (Auth::user()->role->slug == 'ADM')
                    <a href="{{ route('leads.edit', $lead) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('leads.destroy', $lead) }}" method="post" style="display: inline-block">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                @endif
                @if(Auth::user()->role->slug=='ADM'||Auth::user()->role->slug=='TMLD'||Auth::user()->role->slug=='SAL')
                    <a href="{{route('leads.call',$lead)}}"class="btn btn-primary">Call</a>
                @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>



    <!-- Modal -->
    <div class="modal fade" id="modal-assigned" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Assigned Sales Man</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('leads.assigned') }}">
                        <label for="sales">Sales</label>
                        <select name="sales" id="sales" class="form-control">
                            @foreach($sales as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
