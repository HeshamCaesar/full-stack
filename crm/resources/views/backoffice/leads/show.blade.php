@extends('layouts.app')

@section('title', ' Leaders')

@section('pageHeader', 'Show Lead')

@section('content')
    <h3><span class="text-bold">Name:</span> {{ $lead->name }}</h3>
    <p><span class="text-bold">Phone:</span> {{ $lead->phone }}</p>
    <p><span class="text-bold">Email:</span> {{ $lead->email }}</p>
    <p><span class="text-bold">Address:</span> {{ $lead->address }}</p>
    <div>
    @if(\Illuminate\Support\Facades\Auth::user()->role->slug=='ADM')
            <a href="{{ route('leads.edit', $lead) }}" class="btn btn-primary">Edit</a>
            <form action="{{ route('leads.destroy', $lead) }}" method="post" style="display: inline-block">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
    @endif
    </div>

@endsection
