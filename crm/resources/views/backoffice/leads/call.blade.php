@extends('layouts.app')
@section('title','Call Description')
@section('pageHeader','Call Description')
@section('content')
 <div class="card">
            <form action="{{route('calls.store',$lead)}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="description">Call Description</label>
                    <textarea name="description" id="description" cols="30" rows="10" class="form-control "></textarea>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="1">Waiting</option>
                        <option value="2">Approved</option>
                        <option value="3">Rejected</option>
                    </select>
                </div>
                <div class="text-center">
                    <button class="btn btn-success m-3">
                        Save
                    </button>

                </div>
            </form>
     <hr>
        <h3 class="text-center font-weight-normal">History calls</h3>
         <table class=" m-3 table table-stripped table-hover table-bordered">
             <thead>
             <tr>
                 <th>Date</th>
                 <th>User</th>
                 <th>Status</th>
                 <th>Description</th>
             </tr>
             </thead>
             <tbody>
                @foreach($calls as $call)
                    <tr>
                        <td>{{$call->created_at}}</td>
                        <td>{{$call->users->name}}</td>
                        <td>
                            @if($call->status=='1')
                                Waiting
                            @elseif($call->status=='2')
                                Approved
                            @else
                                Rejected
                            @endif
                        </td>
                        <td>{{$call->description}}</td>
                    </tr>
                @endforeach
             </tbody>

         </table>



 </div>

@endsection