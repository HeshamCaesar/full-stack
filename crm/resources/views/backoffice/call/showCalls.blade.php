@extends('layouts.app')
@section('title','All Calls')
@section('pageHeader','All Calls')
@section('content')
    <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
            <th>Users</th>
            <th>Leads</th>
            <th>Status</th>
            <th>Description</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
            @foreach($calls as $call)
                <tr>
                    <td><strong>{{$call->users->name}}</strong></td>
                    <td>{{$call->leads->name}}</td>
                    <td><strong>
                        @if($call->status=='1')
                            Waiting
                        @elseif($call->status=='2')
                            Approved
                        @else
                            Rejected
                        @endif
                        </strong>
                    </td>
                    <td>{{$call->description}}</td>
                    <td>{{$call->created_at}}</td>
                </tr>
                @endforeach
        </tbody>
    </table>

@endsection