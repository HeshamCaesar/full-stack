@extends('layouts.app')

@section('title', 'Users')

@section('pageHeader', 'Edit Users')

@section('content')
    <form action="{{ route('users.update', $user) }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" value="{{ $user->email }}">
        </div>
        <div class="form-group">
            <label for="permission">Permission</label>
            <select name="permission" id="permission" class="form-control">
                <option value="1">ADM</option>
                <option value="2">TMLD</option>
                <option value="3">SAL</option>
            </select>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
@endsection
