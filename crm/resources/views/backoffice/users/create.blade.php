@extends('layouts.app')
@section('title','Create User')
@section('pageHeader','Create Users')
@section('content')

    <form action="{{route('users.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" >
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" >
        </div>
        <div class="form-group">
            <label for="permission">Permission</label>
            <select name="permission" id="permission" class="form-control">
                <option value="3">SAL</option>
                <option value="1">ADM</option>
                <option value="2">TMLD</option>
            </select>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" >
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
@endsection