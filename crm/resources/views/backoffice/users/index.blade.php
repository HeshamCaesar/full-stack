@extends('layouts.app')
@section('title','Users')
@section('pageHeader','All Users')
@section('content')
    <table class="table table-striped table-hover table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Permission</th>
            <th>Created at</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as$user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        @if($user->role_id=='1')
                            ADM
                        @elseif($user->role_id=='2')
                            TMLD
                        @elseif($user->role_id=='3')
                            SAL
                        @endif
                    </td>
                    <td>{{$user->created_at}}</td>
                    <td>
                        <a href="{{ route('users.show', $user) }}" class="btn btn-info">Show
                            <a href="{{ route('users.edit', $user) }}" class="btn btn-primary">Edit</a>
                            <form action="{{ route('users.destroy', $user) }}" method="post" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection