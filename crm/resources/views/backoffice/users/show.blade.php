@extends('layouts.app')

@section('title', 'users')

@section('pageHeader', 'Show User')

@section('content')
    <h3><span class="text-bold">Name:</span> {{ $user->name }}</h3>
    <p><span class="text-bold">Email:</span> {{ $user->email }}</p>
    <p><span class="text-bold">Permission:</span>
        @if($user->role_id=='1')
            ADM
        @elseif($user->role_id=='2')
            TMLD
        @elseif($user->role_id=='3')
            SAL
        @endif</p>
    <div>
        <a href="{{ route('users.edit', $user) }}" class="btn btn-primary">Edit</a>
        <form action="{{ route('users.destroy', $user) }}" method="post" style="display: inline-block">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    </div>

@endsection
