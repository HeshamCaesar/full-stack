@extends('layouts.app')
@section('title','Calls')
@section('pageHeader','My calls')
@section('content')
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>Leader Name</th>
            <th>status</th>
            <th>Call Description</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sales as $sale)
            <tr>
                <td>{{$sale->leads->name}}</td>
                <td>
                    <strong>
                        @if($sale->status=='1')
                            Waiting
                        @elseif($sale->status=='2')
                            Approved
                        @else
                            Rejected
                        @endif
                    </strong>
                </td>
                <td>{{$sale->description}}</td>
                <td>{{$sale->created_at}}</td>
                <td>{{$sale->updated_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection