@extends('layouts.app')
@section('title','leaders')
@section('pageHeader','Your Leaders')
@section('content')
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
        @foreach($sales as $sal)
                <tr>
                    <td>{{$sal->name}}</td>
                    <td>{{$sal->phone}}</td>
                    <td>{{$sal->email}}</td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection