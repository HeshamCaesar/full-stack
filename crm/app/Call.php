<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function leads()
    {
        return $this->belongsTo( Lead::class,'lead_id');
    }
}
