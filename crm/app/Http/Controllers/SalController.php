<?php

namespace App\Http\Controllers;

use App\Call;
use App\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalController extends Controller
{
    public function index()
    {   $user=Auth::user()->id;
        $lead=Lead::where('sales_id',$user)->get();
        return view('backoffice/sales/index',['sales'=>$lead]);
    }

    public function viewCalls()
    {
        $var=Call::where('user_id',Auth::user()->id)->get();
        //dd($var);
        return view('backoffice/sales/myCalls',['sales'=>$var]);
    }
}
