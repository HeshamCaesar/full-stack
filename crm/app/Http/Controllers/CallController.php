<?php

namespace App\Http\Controllers;

use App\Call;
use App\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CallController extends Controller
{
    public function store(Request $request , Lead $lead )
    {
        $call=new Call();
        $call->description=$request->input('description');
        $call->status=$request->input('status');
        $call->user_id=Auth::user()->id;
        $call->lead_id=$lead->id;
        $call->save();
        return redirect()->route('leads.index');
    }

    public function view(Lead $lead)
    {   $calls=Call::all();
        return view('backoffice.call.showCalls')->with('calls',$calls);
    }


}
