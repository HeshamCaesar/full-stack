<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {   $users=User::where('status',1)->orderBy('name')->paginate(10);
        return view('backoffice.users.index',
            ['users'=>$users]);
    }

    public function create()
    {
        return view('backoffice.users.create');
    }

    public function store(UserRequest $request)
    {
        $user=new User();
        $user->name=$request->input('name');
        $user->password=Hash::make($request->input('password'));
        $user->email=$request->input('email');
        $user->role_id=$request->input('permission');
        $user->save();
        return redirect()->route('users.index');
    }
    public function show(User $user)
    {
        return view('backoffice.users.show', ['user'=> $user]);
    }
    public function edit(User $user)
    {
        return view('backoffice.users.edit', ['user'=>$user]);
    }
    public function update(Request $request, User $user)
    {

        $validator = Validator::make($request->all(), [
            'name'=> 'required|string',
            'email'=> [
                'email',
                Rule::unique('users')->ignore($user),
            ]
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role_id = $request->input('permission');

        $user->save();
        return redirect()->route('users.show', $user);
    }
    public function destroy(User $user)
    {    $user->status = false;
        $user->save();
        return redirect()->route('users.index')->with('message', 'Deleted');
    }

}
